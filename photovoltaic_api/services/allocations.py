from datetime import datetime
from odoo.addons.component.core import Component
from odoo.addons.base_rest import restapi
from pydantic import parse_obj_as

from ..pydantic_models.allocation import Allocation
from ..pydantic_models.list_response import ListResponse



class AllocationService(Component):
    _inherit = 'base.rest.service'
    _name = 'allocations.service'
    _usage = 'allocations'
    _collection = 'photovoltaic_api.services'


    @restapi.method(
        [(['/'], 'GET')],
        auth='api_key',
        input_param=restapi.CerberusValidator('_validator_get_allocations'),
        output_param=restapi.PydanticModel(ListResponse[Allocation])
    )
    def search(self, contract_ids=[], offset=0, limit=None):
        domain = []
        if (len(contract_ids) > 0):
            domain.append(('contract_id', 'in', contract_ids))
        
        # Get allocations
        allocations = self.env['account.allocation'].search(domain, limit, offset, order='start_period_date')
        total_allocations = self.env['account.allocation'].search_count(domain)

        # Get liquidation (legacy allocations)
        liquidations = self.env['participant.liquidations'].search(domain, limit, offset, order='period')
        total_liquidations = self.env['participant.liquidations'].search_count(domain)

        rows = []
        for allocation in allocations:
            rows.append(self._allocation_to_json(allocation))

        for liquidation in liquidations:
            rows.append(self._liquidation_to_json(liquidation))

        rows.sort(key=lambda datum: (datum['year'], datum['period']), reverse=True)
        return parse_obj_as(ListResponse[Allocation], {'total': total_allocations+total_liquidations, 'rows': [Allocation.parse_obj(r) for r in rows]})


    # Private methods
    def _calculate_allocation_period(self, start_date, end_date):
        if (end_date.month - start_date.month == 2): #If only two months have passed from start to finish then it is trimestral otherwise is yearly
            return int(end_date.month/3), end_date.year
        return 0, end_date.year

    def _calculate_liquidation_period(self, full_period, payment_period):
        if (payment_period == 'Trimestral'):
            return int(full_period[0]), int(full_period.split(' ')[-1])
        return 0, int(full_period.split(' ')[-1])

    def _calculate_dates_of_period(self, period, year):
        if (period == 1):
            return datetime(year, 1, 1), datetime(year, 3, 31)
        elif (period == 2):
            return datetime(year, 4, 1), datetime(year, 6, 30)
        elif (period == 3):
            return datetime(year, 7, 1), datetime(year, 9, 30)
        elif (period == 4):
            return datetime(year, 10, 1), datetime(year, 12, 31)
        return datetime(year, 1, 1), datetime(year, 12, 31)



    def _calculate_production(self, contract_productions, start_date, end_date):
        energy_generated = 0
        tn_co2_avoided = 0
        eq_family_consumption = 0
        filtered_contract_productions = contract_productions.search([
            ('start_date', '>=', start_date), 
            ('start_date', '<=', end_date)
        ])
        
        for production in filtered_contract_productions:
            energy_generated += production.energy_generated_contract
            tn_co2_avoided += production.tn_co2_avoided_contract
            eq_family_consumption += production.eq_family_consum_contract

        return energy_generated, tn_co2_avoided, eq_family_consumption

    def _allocation_to_json(self, allocation):
        period, year = self._calculate_allocation_period(allocation.start_period_date, allocation.end_period_date)
        energy_generated, tn_co2_avoided, eq_family_consumption = self._calculate_production(allocation.contract_id.contract_production_ids, allocation.start_period_date, allocation.end_period_date)
        return {
            'id': allocation.id,
            'amount': allocation.total,
            'period': period,
            'year': year,
            'state': allocation.state,
            'energy_generated': energy_generated,
            'tn_co2_avoided': tn_co2_avoided,
            'eq_family_consumption': eq_family_consumption,
            'type': 'allocation'
        }

    def _liquidation_to_json(self, liquidation):
        period, year = self._calculate_liquidation_period(liquidation.period, liquidation.payment_period_id.name)
        start_date, end_date = self._calculate_dates_of_period(period, year)
        energy_generated, tn_co2_avoided, eq_family_consumption = self._calculate_production(liquidation.contract_id.contract_production_ids, start_date, end_date)
        return {
            'id': liquidation.id,
            'amount': liquidation.amount,
            'period': period,
            'year': year,
            'state': liquidation.state,
            'energy_generated': energy_generated,
            'tn_co2_avoided': tn_co2_avoided,
            'eq_family_consumption': eq_family_consumption,
            'type': 'liquidation'
        }

    def _validator_get_allocations(self):
        return {
            "contract_ids": {"type": "list", 'items': [{'type': 'integer'}]},
            "offset": {'type': 'integer'},
            'limit': { 'type': 'integer'}
        }

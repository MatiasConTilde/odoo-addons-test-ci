import os
from setuptools_odoo.git_postversion import get_git_postversion, STRATEGY_P1_DEVN
from setuptools_odoo.manifest import NoManifestFound

for dir in os.listdir('setup'):
    try:
        print(dir, get_git_postversion(dir, STRATEGY_P1_DEVN))

        repository = 'testpypi'
        if os.environ.get('CI_COMMIT_TAG') == f'{dir}-{get_git_postversion(dir, STRATEGY_P1_DEVN)}':
            repository = 'testpypi'
            print('main repooooooooooooooooo')

        os.system(f'cd setup/{dir} && python setup.py sdist bdist_wheel --universal && twine upload -r {repository} dist/*')
    except NoManifestFound:
        pass
